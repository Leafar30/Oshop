import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import * as firebase from 'firebase';
import { AppUser } from './models/app-user';

// created this service to give users what they can access or not
@Injectable()
export class UserService {

  constructor(private db: AngularFireDatabase) {

  }
  save(user: firebase.User) {
    // using update method on the end in case user change their name on google
    this.db.object('/users/' + user.uid).update({
      name: user.displayName,
      email: user.email
    });
  }
  // this method is to get an user from firebase.
  // check (model/ app-user.ts) we can anotade now this method here  - FirebaseObjectObservable<AppUser>
  // go back to Auth Guard Admin to finish it off.( AdminAuthGuard )
   get(uid: string): AngularFireObject<AppUser> {
     return this.db.object('/users/' + uid);
   }
}

// (for save method) after creating this service go back to app.component to inject it in.
