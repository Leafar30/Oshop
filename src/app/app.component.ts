import { UserService } from './user.service';
import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
// userService is being injected here when user login to show only what it can be
// accessable by the user (excluded admin users)
  constructor (private userService: UserService,
    private auth: AuthService,
    router: Router) {
    auth.user$.subscribe(user => {
      if (!user) { return; }
      userService.save(user);
// command below to route user back to his original url (ex. login url)
        let returnUrl = localStorage.getItem('returnUrl');
        if (!returnUrl) { return; }
          localStorage.removeItem('returnUrl');
          router.navigateByUrl(returnUrl);
    });
  }
}
