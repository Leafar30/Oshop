import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { ShoppingCartService } from './shopping-cart.service';

@Injectable()
export class OrderService {

  constructor(private db: AngularFireDatabase, private shoppingCart: ShoppingCartService) { }

  async placeOrder(order) {
     let result = await this.db.list('/orders').push(order);
    // this.shoppingCart.clearCart();
     return result;
  }

  getOrders() {
   return  this.db.list('/orders/').valueChanges();
  }

  getOrdersByUsers(userId: string) {
    return this.db.list('/orders',
    ref => ref.orderByChild('userId').equalTo(userId)).valueChanges();
  }
}
