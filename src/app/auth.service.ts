import { UserService } from './user.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { AppUser } from './models/app-user';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class AuthService {

  user$: Observable<firebase.User>;
 // private route: ActivatedRoute - it needs to be added for to route to the right page
  constructor(
    private userService: UserService,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute
  ) {
    this.user$ = afAuth.authState;
  }

  Login() {
    // after the google authenticate it will route the back in the page accordingly
    // tslint:disable-next-line:prefer-const
    let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    localStorage.setItem('returnUrl', returnUrl);
    // signInWithRedirect - return a Promise
    this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
    // go to app.component to redirect a user to the right page.
  }

  Logout() {
    this.afAuth.auth.signOut();
  }
// object and method is to avoid non-admin users see the other links - manage products
// manage orders
  get appUser$(): Observable<AppUser> {
    return this.user$
    .switchMap(user => {
      // fixing issue with the logout method
      if (user) { return this.userService.get(user.uid).valueChanges(); }

      return Observable.of(null);
    });
  }
}
