import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class CategoryService {

  observableCategories$: Observable<any[0]>;

  constructor(private db: AngularFireDatabase) {

    this.observableCategories$ = this.db.list('/categories', ref => ref.orderByChild('name')).snapshotChanges();
}

  getAll() {
     return this.observableCategories$;
  }
}
