import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { RouterStateSnapshot } from '@angular/router/src/router_state';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }
// return true or false - it gets the authentication status of the current user
// RouterStateSnapshot - object {queryParams: with retunrUrl} - can't cheat using http://localhost:4200/check-out
// RouterStateSnapshot  - user is not logged in it will send back to login page..
  canActivate(route, state: RouterStateSnapshot) {
    // with map method we are mapping the an Observable of firebase user$ to an Observable boolean
    // check the documentation Angular.io
    return this.auth.user$.map(user => {
      if (user) {  return true; }

      this.router.navigate(['/login'], {queryParams: { returnUrl: state.url } });
        return false;
    });
  }

}
