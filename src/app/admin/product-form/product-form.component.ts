import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../category.service';
import { ProductService } from '../../product.service';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/take';



@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
// categories$ is an Observable
  categorie$;
  product = <any>{};
  id;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    private productService: ProductService) {

    this.categorie$ = categoryService.getAll();

    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
    this.productService.get(this.id).take(1).subscribe(p => this.product = p.payload.val());
    }
  }

  save(product) {
     if (this.id) { this.productService.update(this.id, product); } else {this.productService.create(product); }

      this.router.navigate(['/admin/products']);
  }

  delete() {
    if (!confirm('Hey Buddy yes or no? Let me know?')) { return; } else {
        this.productService.del(this.id);
      }

      this.router.navigate(['/admin/products']);

  }

  ngOnInit() {
  }

}
