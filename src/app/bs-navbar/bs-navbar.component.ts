
import { ShoppingCart } from './../models/shopping-cart';
import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { AppUser } from '../models/app-user';
import { ShoppingCartService } from '../shopping-cart.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Observable } from 'rxjs/Observable';
import { AngularFireAction, AngularFireList, AngularFireObject } from 'angularfire2/database/interfaces';
import { AngularFirestoreCollection } from 'angularfire2/firestore';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})

export class BsNavbarComponent implements OnInit {
  // avoid browser to crash or keep in constant loop because the Async method that's
  // what you to do create appUser...
  appUser: AppUser;
  // shoppingCartItemCount: number;
   cart$: Observable<ShoppingCart>;

  constructor(private auth: AuthService,
    private shoppingCartService: ShoppingCartService) {}

  async ngOnInit() {
    // to initialize the object using this command below
    this.auth.appUser$.subscribe(appUser => this.appUser = appUser);
    this.cart$ = await this.shoppingCartService.getCart();
    // displaying the products on the top of the shoppingCart
  //   let cart$ = await this.shoppingCartService.getCart();
  //    cart$.valueChanges().subscribe(cart => {
  //  // refratorying the code to show the total items also, in different pages

  //   //   it needs to interate through the items in the firebase
  //     this.shoppingCartItemCount = 0;
  //      for (let productId in cart.items) {
  //        this.shoppingCartItemCount += cart.items[productId].quantity;
  //      }
  //    });
  }

  logout () {
    this.auth.Logout();
  }

}
