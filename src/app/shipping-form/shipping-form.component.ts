import { ShoppingCartService } from './../shopping-cart.service';
import { OrderService } from './../order.service';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Order } from '../models/order';
import { ShoppingCart } from '../models/shopping-cart';

@Component({
  selector: 'shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.css']
})
export class ShippingFormComponent implements OnInit, OnDestroy {
  @Input('cart')cart: ShoppingCart;
  shipping = {};
  userId: string;
  userSubscription: Subscription;

  constructor(
    private router: Router,
    private authService: AuthService,
    private shoppingCartService: ShoppingCartService,
    private orderService: OrderService) {}


  ngOnInit() {
    // linking user order with authentication
    this.userSubscription = this.authService.user$.subscribe(user => this.userId = user.uid);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  async placeOrder() {
    let order = new Order(this.userId, this.shipping, this.cart);
    let result = await this.orderService.placeOrder(order);
  // this code below should be placed in orderService if I need to reuse it;
  // it is not working properly over there. I need to check Transactions
    this.shoppingCartService.clearCart();
// sending user to order-sucess page
// note it is not $key so only key (firebase will store the result on the key)
    this.router.navigate(['/order-success', result.key]);
  }

}
