import { Product } from './product';

export class ShoppingCartItem {

$key: string;
title: string;
imageUrl: string;
price: number;
quantity: number;

// init is an object which looks like a Shopping Cart Object
constructor(init?: Partial<ShoppingCartItem>) {

// this will copy all the proprieties from the init inside this Object.assign
    Object.assign(this, init);
}

 get totalPrice() {return this.price * this.quantity; }
}
