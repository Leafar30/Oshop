// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCs7Za6WcFrbM3fEXLBnGHPWhtADUf3l3E",
    authDomain: "oshop-dc14b.firebaseapp.com",
    databaseURL: "https://oshop-dc14b.firebaseio.com",
    projectId: "oshop-dc14b",
    storageBucket: "oshop-dc14b.appspot.com",
    messagingSenderId: "487845121369"
  }
};
